import enum

from PyQt5 import QtWidgets, QtCore


class ComboBoxDelegate(QtWidgets.QItemDelegate):
    def __init__(self, choices_enum: enum.Enum, parent=None):
        super().__init__(parent)
        self.choices = choices_enum

    def createEditor(self, parent, option, index):
        self.editor = QtWidgets.QComboBox(parent)
        self.editor.addItem('None', None)
        for text, value in self.choices._member_map_.items():
            self.editor.addItem(text, value)
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(QtCore.Qt.EditRole)
        editor_index = self.editor.findData(value)
        if editor_index != -1:
            self.editor.setCurrentIndex(editor_index)

    def setModelData(self, editor, model, index):
        value = self.editor.currentData()
        model.setData(index, value, QtCore.Qt.EditRole)
