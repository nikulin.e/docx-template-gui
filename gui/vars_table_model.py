from PyQt5 import QtCore
from PyQt5.QtCore import QModelIndex, QDate

from domain.template_var import TemplateVar
from domain.var_default_value import VarDefaultValue
from domain.var_type import VarType


class VarsTableModel(QtCore.QAbstractTableModel):
    def __init__(self, *args, template_vars: list[TemplateVar] = None, **kwargs):
        super(VarsTableModel, self).__init__(*args, **kwargs)
        self._template_vars = template_vars

        self._fields = {
            0: self.tr('Name'),
            1: self.tr('Type'),
            2: self.tr('Default value'),
            3: self.tr('Value'),
        }

    def set_var_data(self, template_vars):
        self._template_vars = template_vars

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._template_vars)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self._fields)

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = None):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self._fields[section]

    def data(self, index: QModelIndex, role=None):
        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            template_var = self._template_vars[index.row()]
            if index.column() == 0:
                return template_var.name
            if index.column() == 1:
                return template_var.type.name
            if index.column() == 2:
                return template_var.default_value.value if template_var.default_value else None
            if index.column() == 3:
                if template_var.type == VarType.Date:
                    qd = QDate()
                    if template_var.value:
                        qd.setDate(template_var.value.year, template_var.value.month, template_var.value.day)
                    return qd
                if template_var.type == VarType.Int:
                    return int(0) if not template_var.value else int(template_var.value)
                if template_var.type == VarType.Float:
                    # return float(0) if not template_var.value else float(template_var.value)  # precision limitation of control
                    return str(float(0) if not template_var.value else float(template_var.value))
                return template_var.value

    def setData(self, index: QModelIndex, value, role=None):
        print(index.row(), index.column(), value)
        if role == QtCore.Qt.EditRole:
            template_var = self._template_vars[index.row()]
            if index.column() == 3:
                if template_var.type == VarType.Int:
                    template_var.value = int(value)
                    return True
                if template_var.type == VarType.Float:
                    template_var.value = float(value)
                    return True
                if template_var.type == VarType.Date:
                    template_var.value = value.toPyDate()
                    return True
                template_var.value = value
                return True
            if index.column() == 2:
                if value is None:
                    template_var.default_value = None
                    return True
                if type(value) is VarDefaultValue:
                    template_var.default_value = VarDefaultValue(value)
                    return True
            return False

    def flags(self, index):
        if index.column() in (2, 3):
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable
        if index.column() == 1:
            if not self._template_vars[index.row()].type_from_name:
                return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable
        # return QtCore.Qt.ItemIsEditable
        return QtCore.Qt.ItemIsEnabled
