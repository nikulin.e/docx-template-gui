import enum

from PyQt5 import QtWidgets, QtCore


class TypedComboBoxDelegate(QtWidgets.QItemDelegate):
    def __init__(self, choices_enum: enum.Enum, types_enum: enum.Enum, type_column: int, parent=None):
        super().__init__(parent)
        self.choices = choices_enum
        self.types_enum = types_enum
        self.type_column = type_column

    def createEditor(self, parent, option, index):
        self.editor = QtWidgets.QComboBox(parent)
        self.editor.addItem('None', None)
       # Add typed items
        var_type_str = index.siblingAtColumn(self.type_column).data()
        var_type = dict(self.types_enum._member_map_.items())[var_type_str]
        for text, value in self.choices.items_by_type(var_type).items():
           self.editor.addItem(text, value)
        return self.editor

    def setEditorData(self, editor, index):
        value = index.data(QtCore.Qt.EditRole)
        editor_index = self.editor.findData(value)
        if editor_index != -1:
            self.editor.setCurrentIndex(editor_index)

    def setModelData(self, editor, model, index):
        value = self.editor.currentData()
        model.setData(index, value, QtCore.Qt.EditRole)
