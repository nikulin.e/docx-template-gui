import pathlib

from PyQt5 import QtCore
from PyQt5.QtCore import QModelIndex

from domain.template_options import TemplateOptions


class TemplatesTableModel(QtCore.QAbstractTableModel):
    def __init__(self, *args, templates: list[TemplateOptions] = None, **kwargs):
        super(TemplatesTableModel, self).__init__(*args, **kwargs)
        self._templates = templates
        self._fields = {
            0: self.tr('Template'),
            1: self.tr('Output name'),
        }

    def set_template_data(self, templates):
        self._templates = templates

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._templates)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self._fields)

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = None):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self._fields[section]

    def data(self, index: QModelIndex, role=None):
        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            template = self._templates[index.row()]
            if index.column() == 0:
                return pathlib.Path(template.template_path).name
            else:
                return template.output_name
        if role == QtCore.Qt.ToolTipRole and index.column() == 0:
            template = self._templates[index.row()]
            return template.template_path

    def setData(self, index: QModelIndex, value, role=None):
        if role == QtCore.Qt.EditRole:
            self._templates[index.row()].output_name = value
            return True

    def flags(self, index):
        if index.column() == 1:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable
        return QtCore.Qt.ItemIsEditable
