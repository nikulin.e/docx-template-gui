import pathlib

import markdown
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QFileDialog, QAction, QMessageBox

from config import Config
from domain.var_default_value import VarDefaultValue
from domain.var_type import VarType
from gui.templates_table_model import TemplatesTableModel
from gui.typed_combo_box_delegate import TypedComboBoxDelegate
from gui.vars_table_model import VarsTableModel
from help_window import HelpWindow
from renderer.template_renderer import TemplateRenderer
from ui_main_window import Ui_MainWindow
from domain.project import TemplateProject
from domain.template_options import TemplateOptions
from utils import open_file_associated, delayed_remove_file


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, config: Config, app_name: str):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.retranslateUi(self)

        # App Config and App name
        self._config = config
        self._app_name = app_name

        # Project ref
        self._project = TemplateProject()
        self._project_path = None
        self._recent_actions = []

        # Table templates
        self._template_model = TemplatesTableModel(templates=self._project.templates)
        self.ui.tvTemplates.setModel(self._template_model)

        # Table vars
        self._vars_model = VarsTableModel(template_vars=self._project.templates_vars)
        self.ui.tvTemplatesVars.setModel(self._vars_model)
        self.ui.tvTemplatesVars.setItemDelegateForColumn(2, TypedComboBoxDelegate(VarDefaultValue, VarType, 1))

        # Signals
        self.ui.actionNew.triggered.connect(self.new_project)
        self.ui.actionSave.triggered.connect(self.save_project)
        self.ui.actionSave_as.triggered.connect(self.save_as_project)
        self.ui.actionOpen.triggered.connect(self.open_project)
        self.ui.btnAddTemplate.clicked.connect(self.add_template_clicked)
        self.ui.btnRemoveTemplate.clicked.connect(self.remove_template_clicked)
        self.ui.btnRefreshVars.clicked.connect(self.refresh_vars_table)
        self.ui.btnGenerate.clicked.connect(self.generate)
        self.ui.btnPreview.clicked.connect(self.preview)
        self.ui.leOutputPath.textEdited.connect(self.output_path_edited)
        self.ui.btnSelectOutputPath.clicked.connect(self.select_output_path_clecked)
        self.ui.cbOpenRendered.stateChanged.connect(self.open_rendeder_triggered)
        self.ui.actionTemplateTags.triggered.connect(self.open_help)

        # Start with new project
        self.new_project()
        self.update_recent_menu()
        self.update_title()


    def add_template_clicked(self):
        file_path, _ = QFileDialog.getOpenFileName(
            caption=self.tr('Select template file'),
            filter=self.tr("Docx file (*.docx);;All files (*.*)"),
        )
        if not file_path:
            return
        file_name = pathlib.Path(file_path).name
        # Append new template
        template = TemplateOptions(template_path=file_path, output_name=file_name)
        self._project.templates.append(template)
        # Update UI
        self._template_model.layoutChanged.emit()
        self.ui.tvTemplates.resizeColumnsToContents()
        self.refresh_vars_table()


    def remove_template_clicked(self):
        selected_row = self.ui.tvTemplates.selectionModel().currentIndex().row()
        if selected_row >= 0:
            self._project.templates.remove(self._project.templates[selected_row])
            self._template_model.layoutChanged.emit()
            self.refresh_vars_table()

    def new_project(self):
        self._project_path = None
        self._project = TemplateProject()
        self._project.name = 'Template project'
        # TODO: Clean all controls
        self._template_model.set_template_data(self._project.templates)
        self._template_model.layoutChanged.emit()
        self.refresh_vars_table()
        self.ui.leOutputPath.setText(self._project.output_options.output_path)
        self.ui.cbOpenRendered.setChecked(self._project.output_options.open_after_render)


    def load_project(self, file_path):
        new_proj = TemplateProject.load(pathlib.Path(file_path))
        self._project_path = pathlib.Path(file_path)
        self._project = new_proj
        # TODO: Clean all controls
        self._template_model.set_template_data(self._project.templates)
        self._template_model.layoutChanged.emit()
        self.ui.tvTemplates.resizeColumnsToContents()
        self.refresh_vars_table()
        self.ui.leOutputPath.setText(self._project.output_options.output_path)
        self.ui.cbOpenRendered.setChecked(self._project.output_options.open_after_render)
        # Update config and UI
        self.update_config()
        self.update_recent_menu()
        self.update_title()

    def save_project(self):
        if self._project_path is None:
            return self.save_as_project()
        else:
            self._project.save(self._project_path)

    def save_as_project(self):
        # TODO: default ext
        #  QFileDialog.setDefaultSuffix('tp')
        file_path, _ = QFileDialog.getSaveFileName(
            caption=self.tr('Save project file'),
            filter=self.tr('Template Project (*.tpr);;All files (*.*)'),
        )
        if not file_path:
            return
        self._project_path = pathlib.Path(file_path)
        self._project.save(self._project_path)
        self.update_config()
        self.update_recent_menu()
        self.update_title()

    def open_project(self):
        file_path, _ = QFileDialog.getOpenFileName(
            caption=self.tr('Select project file'),
            filter=self.tr('Template Project (*.tpr);;All files (*.*)'),
        )
        if not file_path:
            return
        self.load_project(file_path)


    def update_config(self):
        # update recent projects
        self._config.last_project_path = str(self._project_path)
        if self._config.last_project_path in self._config.projects_paths:
            self._config.projects_paths.remove(self._config.last_project_path)
        self._config.projects_paths.insert(0, self._config.last_project_path)

        self._config.save()

    def update_recent_menu(self):
        # Clear menu
        for act in self._recent_actions:
            self.ui.menuRecent.removeAction(act)
        # Fill
        self._recent_actions = []

        def get_open_handler(any_path: str):
            return lambda: self.load_project(any_path)

        for index, proj_path in enumerate(self._config.projects_paths):
            act = QAction(pathlib.Path(proj_path).name)
            act.triggered.connect(get_open_handler(self._config.projects_paths[index]))
            self._recent_actions.append(act)
            self.ui.menuRecent.addAction(act)
        #self.ui.menuRecent.addActions(self._recent_actions)

    def update_title(self):
        title = self.tr(self._app_name)
        if self._project_path:
            title += ' - ' + pathlib.Path(self._project_path).name
        elif self._project and self._project.name:
            title += ' - ' + self._project.name
        self.setWindowTitle(title)

    def refresh_vars_table(self):
        self._project.update_template_vars()
        self._project.fill_templates_vars()
        self._vars_model.set_var_data(self._project.templates_vars)
        self._vars_model.layoutChanged.emit()
        self.ui.tvTemplatesVars.resizeColumnsToContents()

    def generate(self):
        context = self._project.get_context()
        out_path_dir = pathlib.Path('')
        if self._project.output_options.output_path:
            rendered_path = TemplateRenderer.render_string(self._project.output_options.output_path, context)
            out_path_dir = pathlib.Path(rendered_path)
            if out_path_dir.exists():
                ret = QMessageBox.question(self, self._app_name,
                                           'Output dir already exists. Are you sure to generate files to existing dir?',
                                           QMessageBox.Yes | QMessageBox.No)
                if ret != QMessageBox.Yes:
                    return

        for template in self._project.templates:
            if not template.output_name:
                continue
            rendered_path = TemplateRenderer.render_string(template.output_name, context)
            out_path = out_path_dir.joinpath(rendered_path)

            if out_path.exists():
                ret = QMessageBox.question(self, self._app_name,
                                           f'Output file {out_path} already exists. Are you sure to override it?',
                                           QMessageBox.Yes | QMessageBox.No)
                if ret != QMessageBox.Yes:
                    continue

            if not out_path.parent.exists():
                out_path.parent.mkdir(parents=True)

            TemplateRenderer.render_template(pathlib.Path(template.template_path), out_path, context)
            if self._project.output_options.open_after_render:
                open_file_associated(out_path)

    def preview(self):
        context = self._project.get_context()
        for template in self._project.templates:
            preview_file_path = TemplateRenderer.preview_template(pathlib.Path(template.template_path), context)
            open_file_associated(preview_file_path)
            delayed_remove_file(preview_file_path)

    # Output options
    def select_output_path_clecked(self):
        init_dir = str(pathlib.Path.home())
        # set from proj dir
        if self._project_path and pathlib.Path(self._project_path).parent.exists():
            init_dir = str(pathlib.Path(self._project_path).parent)
        # set from current path
        if self.ui.leOutputPath.text():
            current_path = self.ui.leOutputPath.text().split('{')[0]
            if pathlib.Path(current_path).exists():
                init_dir = current_path
        dir_path = QFileDialog.getExistingDirectory(
            caption=self.tr('Select output dir'),
            directory=init_dir,
        )
        if not dir_path:
            return
        self.ui.leOutputPath.setText(dir_path)
        self._project.output_options.output_path = dir_path

    def output_path_edited(self, new_path):
        self._project.output_options.output_path = new_path

    def open_rendeder_triggered(self, state: int):
        self._project.output_options.open_after_render = state == QtCore.Qt.Checked

    # Help
    def open_help(self):
        self.help_window = HelpWindow()
        self.help_window.show()