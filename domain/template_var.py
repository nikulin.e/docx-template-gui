import typing
from dataclasses import dataclass, field
from typing import Optional

from serde import deserialize, serialize

from domain.var_default_value import VarDefaultValue
from domain.var_type import VarType


@deserialize
@serialize
@dataclass
class TemplateVar:

    @staticmethod
    def from_name(name: str):
        new_template_var = TemplateVar(name=name)
        name_splits = name.split('_')
        if name_splits[0] in VarType._value2member_map_:
            new_template_var.type = VarType(name_splits[0])
            new_template_var.type_from_name = True
        else:
            new_template_var.type = VarType.String
        return new_template_var

    name: Optional[str] = None
    type: Optional[VarType] = None
    type_from_name: Optional[bool] = False
    default_value: Optional[VarDefaultValue] = VarDefaultValue.NoneValue

    value: typing.Any = field(default=None, metadata={'serde_skip': True})



