from dataclasses import dataclass
from typing import Optional

from serde import serialize, deserialize


@deserialize
@serialize
@dataclass
class TemplateOptions:
    template_path: Optional[str] = None
    output_name: Optional[str] = None


