from datetime import date
from enum import Enum


class VarType(Enum):
    String = str.__name__
    Int = int.__name__
    Float = float.__name__
    Date = date.__name__
