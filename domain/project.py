import datetime
import pathlib
from dataclasses import dataclass, field
from typing import Optional

from serde import serialize, deserialize
from serde.yaml import from_yaml, to_yaml

from domain.output_options import OutputOptions
from domain.template_options import TemplateOptions
from domain.template_var import TemplateVar
from domain.var_default_value import VarDefaultValue
from domain.var_type import VarType
from renderer.template_renderer import TemplateRenderer


@deserialize
@serialize
@dataclass
class TemplateProject:

    @staticmethod
    def load(file_path: pathlib.Path):
        if not file_path.exists():
            return TemplateProject()
        with open(file_path, "r") as f:
            return from_yaml(TemplateProject, f.read())

    def save(self, file_path: pathlib.Path):
        if not file_path.parent.exists():
            file_path.parent.mkdir()
        with open(file_path, "w",) as f:
            return f.write(to_yaml(self))

    def update_template_vars(self):
        new_templates_vars = []
        for template in self.templates:
            template_vars = TemplateRenderer.get_templates_vars(pathlib.Path(template.template_path))
            for template_var_name in template_vars:
                exists_var = list(filter(lambda x: x.name == template_var_name, new_templates_vars))
                if len(exists_var) > 0:
                    # already in new list, continue
                    continue
                exists_var = list(filter(lambda x: x.name == template_var_name, self.templates_vars))
                if len(exists_var) > 0:
                    # already in old list, simple copy
                    new_templates_vars.append(list(exists_var)[0])
                else:
                    # new var. create and append
                    template_var = TemplateVar.from_name(template_var_name)
                    new_templates_vars.append(template_var)
        new_templates_vars.sort(key=lambda x: x.name)
        self.templates_vars = new_templates_vars

    def fill_templates_vars(self):
        for template_var in self.templates_vars:
            # if template_var.value is not None:
            #     continue
            # special default value
            if type(template_var.default_value) is VarDefaultValue \
                    and template_var.default_value.name in VarDefaultValue.items_by_type(template_var.type).keys():
                today = datetime.date.today()
                zero_values = {
                    VarDefaultValue.Today: today,
                    VarDefaultValue.CurrentMonth: today.month,
                    VarDefaultValue.CurrentYear: today.year,
                    VarDefaultValue.CurrentMonthStart: today.replace(day=1),
                    VarDefaultValue.CurrentMonthEnd: (today.replace(day=28) + datetime.timedelta(days=5)).replace(day=1) - datetime.timedelta(days=1),
                    VarDefaultValue.PreviousMonthStart: (today.replace(day=1) - datetime.timedelta(days=1)).replace(day=1),
                    VarDefaultValue.PreviousMonthEnd: today.replace(day=1) - datetime.timedelta(days=1),
                }
                template_var.value = zero_values[template_var.default_value]
            # simple default value (NOT POSSIBLE NOW)
            # elif template_var.default_value is not None:
            #     values = {
            #         'str': str(template_var.default_value),
            #         'int': int(template_var.default_value),
            #         'float': float(template_var.default_value),
            #         'date': datetime.date.fromisoformat(template_var.default_value),
            #     }
            #     template_var.value = values[template_var.type]
            # zero values
            if template_var.value is None:
                zero_values = {
                    VarType.String: '',
                    VarType.Int: 0,
                    VarType.Float: 0.0,
                    VarType.Date: datetime.date.today(),
                }
                template_var.value = zero_values[template_var.type]


    def get_context(self):
        context = dict((x.name, x.value) for x in self.templates_vars)
        return context

    # Project fields
    name: Optional[str] = None
    templates: Optional[list[TemplateOptions]] = field(default_factory=list)
    templates_vars: Optional[list[TemplateVar]] = field(default_factory=list)
    output_options: Optional[OutputOptions] = field(default_factory=lambda: OutputOptions())

