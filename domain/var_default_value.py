from enum import Enum

from domain.var_type import VarType


class VarDefaultValue(Enum):
    Today = 'today'
    CurrentMonth = 'current_month'
    CurrentYear = 'current_year'
    CurrentMonthStart = 'current_month_start'
    CurrentMonthEnd = 'current_month_end'
    PreviousMonthStart = 'previous_month_start'
    PreviousMonthEnd = 'previous_month_end'

    NoneValue = 'none_value'

    @classmethod
    def all_items(cls) -> dict:
        return cls._member_map_.items()

    @classmethod
    def items_by_type(cls, var_type: VarType) -> dict:
        items = {
            VarType.Int: {
                cls.CurrentMonth.name: cls.CurrentMonth,
                cls.CurrentYear.name: cls.CurrentYear,
            },
            VarType.Date: {
                cls.Today.name: cls.Today,
                cls.CurrentMonthStart.name: cls.CurrentMonthStart,
                cls.CurrentMonthEnd.name: cls.CurrentMonthEnd,
                cls.PreviousMonthStart.name: cls.PreviousMonthStart,
                cls.PreviousMonthEnd.name: cls.PreviousMonthEnd,
            },
            VarType.Float: {},
            VarType.String: {},
        }

        return items[var_type]
