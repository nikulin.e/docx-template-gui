from dataclasses import dataclass
from typing import Optional

from serde import serialize, deserialize


@deserialize
@serialize
@dataclass
class OutputOptions:
    output_path: Optional[str] = None
    open_after_render: Optional[bool] = False

