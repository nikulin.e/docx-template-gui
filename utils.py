import os
import platform
import subprocess
import threading
from time import sleep


def open_file_associated(out_path: str):
    # fix https://github.com/pyinstaller/pyinstaller/issues/3668
    my_env = dict(os.environ)
    lp_key = 'LD_LIBRARY_PATH'
    lp_orig = my_env.get(lp_key + '_ORIG')
    if lp_orig is not None:
        my_env[lp_key] = lp_orig
    else:
        lp = my_env.get(lp_key)
        if lp is not None:
            my_env.pop(lp_key)
    # QT_PLUGIN_PATH override by pyqt in pyinstaller (

    if platform.system() == 'Darwin':  # macOS
        subprocess.call(('open', out_path))
    elif platform.system() == 'Windows':  # Windows
        os.startfile(out_path)
    else:  # linux variants
        subprocess.call(('xdg-open', out_path), env=my_env)


def delayed_remove_file(file_path: str):
    def df(rm_file_path):
        sleep(30)
        os.remove(rm_file_path)

    threading.Thread(target=df, args=(file_path,)).start()