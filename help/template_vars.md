# Template vars

## Переменные 
Шаблоны документов могут содержать переменные и фильтры в следующем форматах:  
```
{{ Имя_переменной }}
{{ тип_Имя_переменной }}
{{ тип_Имя_переменной|имя_фильтра }}
```

Например:
```
{{ Номер_акта }}
{{ date_Дата_акта }}
{{ int_Сумма|num2text }}
```
## Тип переменных
Если указан тип переменной, он автоматически распознается шаблонизатором и будет корректно задаваться при генерации 
итогового документа. Поэтому крайне желательно указывать тип переменной в самом шаблоне. 
Если тип переменной не задан, считается что это переменная типа **str**.  
Доступные типы переменных, которые распознает шаблонизатор:

 - str
 - date
 - int
 - float
 
## Фильтры
Для разных типов переменных доступны разные типы фильтров. 

### Фильтры для типа `str`

Название  |  Пример
--------- |  ------
capitalize  | {{ 'компания терра'\|capitalize }} -> 'Компания терра'
center(int)  | {{ 'компания'\|center(12) }} -> '  компания  '
indent(int? width, bool? first) | {{ 'компания\\nтерра'\|indent(4) }} -> '  компания\\n    терра'
lower  | {{ 'Компания ТЕРРА'\|lower }} -> 'компания терра'
replace(str, str) | {{ "Hello World"\|replace("Hello", "Goodbye") }} -> Goodbye World
reverse | {{ 'компания'\|reverse }} -> 'яинапмок'
title | {{ 'компания терра'\|title }} -> 'Компания Терра'
trim(str?) | {{ ' компания терра   '\|trim }} -> 'компания терра'
truncate(int len, bool? killwords) | {{ "foo bar baz qux"\|truncate(9) }} -> "foo..."
upper | {{ 'компания терра'\|upper }} -> 'КОМПАНИЯ ТЕРРА'
wordcount | {{ 'компания терра'\|wordcount }} -> 2
wordwrap(int width, bool? break_long_words) | {{ 'компания терра'\|wordwrap(10) }} -> 'компания\\nтерра'

### Фильтры для типа `date`

Название  |  Пример
--------- |  ------
date_short_ru  | {{ date_Важная_дата\|date_short_ru }} -> '10.04.2011'
date_full_ru  | {{ date_Дата_акта\|date_full_ru }} -> '«08» октября 2021'

### Фильтры для типа `int`

Название  |  Пример
--------- |  ------
num2text  | {{ 110\|num2text }} -> 'сто десять'
rub_agree  | {{ 132\|rub_agree }} -> 'рубля'

### Фильтры для типа `float`

Название  |  Пример
--------- |  ------
round(int? precision)  | {{ 17.7\|round }} -> 17.0

### Фильтры для всех типов

Название  |  Пример
--------- |  ------
default(any)  | {{ Опциональная_переменная\|default('Договор №1') }} -> 'Договор №1'
float | {{ '0.7'\|float }} -> 0.7   # или 0.0
int | {{ '18'\|int }} -> 18   # или 0
length | {{ 'компания'\|length }} -> 8
string | {{ 15\|string }} -> '15'


