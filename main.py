import pathlib
import sys
import os

import click
from PyQt5 import QtWidgets, uic
from click_default_group import DefaultGroup
from appdirs import user_config_dir

from config import Config

APP_NAME = 'DocxTemplateGui'
APP_DEF_CONFIG_PATH = pathlib.Path(user_config_dir(APP_NAME)).joinpath('config.yaml')

# fix pymorphy2_dicts_ru in bundle
if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    os.environ["PYMORPHY2_DICT_PATH"] = str(pathlib.Path(sys._MEIPASS).joinpath('pymorphy2_dicts_ru/data'))


@click.group(cls=DefaultGroup, default='run-gui', default_if_no_args=True)
@click.option('-c', '--config', type=pathlib.Path, default=APP_DEF_CONFIG_PATH, help='Configuration file path')
@click.pass_context
def cli(ctx, config):
    ctx.obj['config_path'] = config



@cli.command()
@click.pass_context
def run_gui(ctx):
    config_path = ctx.obj['config_path']
    click.echo(f'Starting {APP_NAME}...')
    click.echo(f'Use config file path: {config_path}')
    config = Config.load(config_path)

    from main_window import MainWindow
    app = QtWidgets.QApplication([])
    win = MainWindow(config, APP_NAME)
    win.show()
    win.setWindowTitle(APP_NAME)
    sys.exit(app.exec())

# @cli.command()
# def dropdb():
#     click.echo('Dropped the database')


if __name__ == '__main__':
    cli(obj={})
