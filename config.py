import pathlib
from dataclasses import dataclass, field
from typing import Optional

from serde import serialize, deserialize
from serde.yaml import from_yaml, to_yaml


@deserialize
@serialize
@dataclass
class Config:
    @staticmethod
    def load(file_path: pathlib.Path):
        if not file_path.exists():
            new_inst = Config()
            new_inst._self_path = file_path
            return new_inst

        with open(file_path, "r") as f:
            new_inst = from_yaml(Config, f.read())
            new_inst._self_path = file_path
            return new_inst

    def save(self, file_path: pathlib.Path = None):
        if not file_path:
            file_path = self._self_path
        if not file_path.parent.exists():
            file_path.parent.mkdir()
        with open(file_path, "w",) as f:
            return f.write(to_yaml(self))

    _self_path: Optional[str] = field(default=None, metadata={'serde_skip': True})

    # Config fields
    last_project_path: Optional[str] = None
    projects_paths: Optional[list[str]] = field(default_factory=list)
