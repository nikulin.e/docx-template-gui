from datetime import datetime

from babel.dates import format_date


# date -> 10.04.2011
def date_short_ru(value: datetime.date) -> str:
    return value.strftime('%d.%m.%Y')


# date -> «08» октября 2021
def date_full_ru(value: datetime.date) -> str:
    return format_date(value, format='«dd» MMMM yyyy', locale='ru')
