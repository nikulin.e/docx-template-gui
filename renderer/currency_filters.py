import pymorphy2
from pymorphy2_dicts_ru.version import __version__

from num2t4ru import num2text

morph = pymorphy2.MorphAnalyzer()


# 130 -> 'рублей'
# 131 -> 'рубль'
# 132 -> 'рубля'
def rub_agree(value: int) -> str:
    rub = morph.parse('рубль')[0]
    r_agree = rub.make_agree_with_number(value).word
    return r_agree


# 110 -> 'сто десять'
# num2text