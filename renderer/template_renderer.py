import os
import pathlib
import tempfile
import typing

import jinja2
from docx.enum.text import WD_COLOR_INDEX
from docxtpl import DocxTemplate

from .currency_filters import rub_agree, num2text
from .date_filters import date_short_ru, date_full_ru


class TemplateRenderer:

    @classmethod
    def _get_jinja_env(cls):
        jinja_env = jinja2.Environment()
        jinja_env.filters['date_short_ru'] = date_short_ru
        jinja_env.filters['date_full_ru'] = date_full_ru
        jinja_env.filters['rub_agree'] = rub_agree
        jinja_env.filters['num2text'] = num2text
        return jinja_env

    @classmethod
    def get_templates_vars(cls, template_path: pathlib.Path):
        doc = DocxTemplate(template_path)
        vars = doc.get_undeclared_template_variables(jinja_env=cls._get_jinja_env())
        return vars

    @classmethod
    def render_template(cls, template_path: pathlib.Path, output_path: pathlib.Path, context: dict[typing.Any]):
        doc = DocxTemplate(template_path)
        doc.render(context, jinja_env=cls._get_jinja_env())
        doc.save(output_path)

    @classmethod
    def render_string(cls, template: str, context: dict[typing.Any]) -> str:
        jinja_env = cls._get_jinja_env()
        jinja_template = jinja_env.from_string(template)
        rendered = jinja_template.render(context)
        return rendered

    @classmethod
    def preview_template(cls, template_path: pathlib.Path, context: dict[typing.Any]):
        doc = DocxTemplate(template_path).get_docx()
        for paragraph in doc.paragraphs:
            for r in paragraph.runs:
                if '{{' in r.text:
                    r.font.highlight_color = WD_COLOR_INDEX.YELLOW
        for table in doc.tables:
            for row in table.rows:
                for cell in row.cells:
                    for paragraph in cell.paragraphs:
                        for r in paragraph.runs:
                            if '{{' in r.text:
                                r.font.highlight_color = WD_COLOR_INDEX.YELLOW
        tmp_fname = tempfile.mktemp() + '.docx'
        doc.save(tmp_fname)
        # render from temp
        preview_path = tempfile.mktemp() + '.docx'
        doc = DocxTemplate(tmp_fname)
        doc.render(context, jinja_env=cls._get_jinja_env())
        doc.save(preview_path)

        os.remove(tmp_fname)

        return preview_path
