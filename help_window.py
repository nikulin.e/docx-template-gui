import os
import pathlib
import sys

import markdown
from PyQt5 import QtWidgets
from ui_help_window import Ui_Dialog

TEMPLATE = """<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
{{content}}
</div>
</body>
</html>
"""


class HelpWindow(QtWidgets.QDialog):
    def __init__(self):
        super(HelpWindow, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.retranslateUi(self)

        self.load_help_md()


    def load_help_md(self):
        md_path = self.get_help_path().joinpath('template_vars.md')
        with open(md_path, 'r') as input_file:
            text = input_file.read()
        extensions = [
            'markdown.extensions.fenced_code',
            'markdown.extensions.tables',
            'markdown.extensions.sane_lists',
        ]
        html = markdown.markdown(text, extensions=extensions)
        self.ui.webEngineView.setHtml(TEMPLATE.replace('{{content}}', html.replace('<table', '<table class="table"')))

    def get_help_path(self):
        if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
            base_path = pathlib.Path(sys._MEIPASS)
        else:
            base_path = pathlib.Path(__file__).absolute().parent
        return base_path.joinpath('help')
